import cadquery as cq
from math import sin, cos, pi, inf


def offset3d(obj, distance: float):
    return obj.shell(distance).union(obj)


def bottom_interface(
        base_width: float,
        base_length: float,
        corner_radius: float,
        wall_clearance: float,
        ridge_width: float,
        ridge_height: float,
        ridge_angle: float,
        **unused_kwargs
):
    result = cq.Workplane('XY') \
        .box(base_width, base_length, ridge_height + wall_clearance, centered=(True, True, False))

    outline = cq.Workplane('XY') \
        .moveTo(-base_width / 2 + corner_radius, -base_length / 2) \
        .lineTo(base_width / 2 - corner_radius, -base_length / 2) \
        .radiusArc((base_width / 2, -base_length / 2 + corner_radius), -corner_radius) \
        .lineTo(base_width / 2, base_length / 2 - corner_radius) \
        .radiusArc((base_width / 2 - corner_radius, base_length / 2), -corner_radius) \
        .lineTo(-base_width / 2 + corner_radius, base_length / 2) \
        .radiusArc((-base_width / 2, base_length / 2 - corner_radius), -corner_radius) \
        .lineTo(-base_width / 2, -base_length / 2 + corner_radius) \
        .radiusArc((-base_width / 2 + corner_radius, -base_length / 2), -corner_radius) \
        .wire()

    profile = cq.Workplane('XZ') \
        .moveTo(0, ridge_height+wall_clearance)\
        .lineTo(ridge_width / 2, ridge_height + wall_clearance)\
        .lineTo(ridge_width / 2, ridge_height) \
        .lineTo(ridge_width / 2 + ridge_height * cos(ridge_angle * pi / 180), 0) \
        .lineTo(-base_width, 0) \
        .lineTo(-base_width, ridge_height + wall_clearance) \
        .close()

    negative = profile.sweep(outline.translate((base_width / 2, 0))).translate((-base_width / 2, 0))

    result = result.cut(negative)

    return result


def container(
        width: int,
        length: int,
        height: int,
        base_width: float,
        base_length: float,
        base_height: float,
        corner_radius: float,
        wall_thickness: float,
        wall_clearance: float,
        bottom_thickness: float,
        ridge_width: float,
        ridge_height: float,
        ridge_angle: float,
        ridge_clearance: float,
        **unused_kwargs
):
    overall_width = base_width * width
    overall_length = base_length * length
    overall_height = base_height * height

    result = cq.Workplane('XY').workplane(offset=ridge_height + wall_clearance) \
        .box(overall_width - wall_clearance, overall_length - wall_clearance, overall_height - wall_clearance,
             centered=(True, True, False)) \
        .edges('|Z') \
        .fillet(corner_radius) \
        .edges('<Z') \
        .chamfer(ridge_width / 2 - wall_clearance / 2)

    inside = cq.Workplane('XY').workplane(offset=ridge_height + bottom_thickness) \
        .box(overall_width - wall_thickness * 2, overall_length - wall_thickness * 2,
             overall_height - bottom_thickness, centered=(True, True, False)) \
        .edges('|Z or <Z') \
        .fillet(corner_radius - wall_thickness)

    result = result.cut(inside)

    bottom = bottom_interface(
        base_width,
        base_length,
        corner_radius,
        wall_thickness,
        ridge_width,
        ridge_height,
        ridge_angle,
    )

    top = offset3d(bottom, ridge_clearance)

    for i in range(0, width):
        for j in range(0, length):
            result = result.union(bottom.translate((
                -overall_width / 2 + base_width / 2 + i * base_width,
                -overall_length / 2 + base_length / 2 + j * base_length,
            )))

            result = result.cut(top.translate((
                -overall_width / 2 + base_width / 2 + i * base_width,
                -overall_length / 2 + base_length / 2 + j * base_length,
                overall_height,
            )))

    return result


def container_grid(
        width: int,
        length: int,
        base_width: float,
        base_length: float,
        corner_radius: float,
        wall_clearance: float,
        ridge_width: float,
        ridge_height: float,
        ridge_angle: float,
        ridge_clearance: float,
        **unused_kwargs
):
    overall_width = width * base_width
    overall_length = length * base_length

    result = cq.Workplane('XY') \
        .box(overall_width, overall_length, ridge_height - ridge_clearance, centered=(True, True, False)) \
        .edges('|Z') \
        .fillet(corner_radius)

    bottom = offset3d(
        bottom_interface(
            base_width,
            base_length,
            corner_radius,
            wall_clearance,
            ridge_width,
            ridge_height,
            ridge_angle,
        ),
        ridge_clearance
    )

    for i in range(0, width):
        for j in range(0, length):
            result = result.cut(bottom.translate((
                -overall_width / 2 + base_width / 2 + i * base_width,
                -overall_length / 2 + base_length / 2 + j * base_length,
            )))

    return result


def drawer(
        width: int,
        length: int,
        height: int,
        base_width: float,
        base_length: float,
        base_height: float,
        corner_radius: float,
        wall_thickness: float,
        wall_clearance: float,
        ridge_width: float,
        ridge_height: float,
        ridge_angle: float,
        ridge_clearance: float,
        handle_thickness: float,
        handle_depth: float,
        handle_height: float,
        tab_height: float,
        side_dip: float,
        **unused_kwargs
):
    overall_width = width * base_width + wall_thickness * 2
    overall_length = length * base_length + wall_thickness * 2
    overall_height = height * base_height + ridge_height

    result = cq.Workplane('XY') \
        .box(overall_width, overall_length, overall_height, centered=(True, True, False)) \
        .edges('|Z') \
        .fillet(corner_radius + wall_thickness)

    side_wall_dip = result \
        .faces('<X') \
        .workplane() \
        .moveTo(-overall_length / 2 + wall_thickness + corner_radius, overall_height) \
        .lineTo(-overall_length / 2 + wall_thickness + corner_radius + side_dip, overall_height - side_dip) \
        .lineTo(overall_length / 2 - wall_thickness - corner_radius - side_dip, overall_height - side_dip) \
        .lineTo(overall_length / 2 - wall_thickness - corner_radius, overall_height) \
        .close() \
        .extrude(-overall_width, combine=False)
    result = result.cut(side_wall_dip)

    inside = cq.Workplane('XY').workplane(offset=ridge_height) \
        .box(overall_width - wall_thickness * 2 + wall_clearance, overall_length - wall_thickness * 2 + wall_clearance,
             overall_height - ridge_height, centered=(True, True, False)) \
        .edges('|Z') \
        .fillet(corner_radius + wall_clearance)

    result = result.cut(inside)

    bottom_inside = cq.Workplane('XY') \
        .box(overall_width - wall_thickness * 2, overall_length - wall_thickness * 2, overall_height - ridge_height,
             centered=(True, True, False)) \
        .edges('|Z') \
        .fillet(corner_radius + wall_clearance / 2)

    result = result.cut(bottom_inside)

    bottom = container_grid(
        width,
        length,
        base_width,
        base_length,
        corner_radius,
        wall_thickness,
        ridge_width,
        ridge_height,
        ridge_angle,
        ridge_clearance,
    )

    result = result.union(bottom)

    handle = cq.Workplane('YZ').workplane(offset=-overall_width / 2 + wall_thickness + corner_radius) \
        .moveTo(overall_length / 2, overall_height) \
        .lineTo(overall_length / 2, overall_height - handle_height * 2) \
        .lineTo(overall_length / 2 + handle_depth, overall_height - handle_height) \
        .close() \
        .extrude(overall_width - wall_thickness * 2 - corner_radius * 2)

    handle_inside = cq.Workplane('YZ').workplane(
        offset=-overall_width / 2 + wall_thickness + corner_radius + handle_thickness) \
        .moveTo(overall_length / 2, overall_height - handle_thickness) \
        .lineTo(overall_length / 2, overall_height - handle_height * 2) \
        .lineTo(overall_length / 2 + handle_depth, overall_height - handle_height * 2) \
        .lineTo(overall_length / 2 + handle_depth, overall_height - handle_height - handle_thickness) \
        .close() \
        .extrude(overall_width - wall_thickness * 2 - corner_radius * 2 - handle_thickness * 2)

    handle = handle.cut(handle_inside)

    result = result.union(handle)

    tab = result.faces('<Y').workplane() \
        .moveTo(overall_width / 2 - wall_thickness - corner_radius, overall_height) \
        .lineTo(overall_width / 2 - wall_thickness - corner_radius - tab_height, overall_height + tab_height) \
        .lineTo(-overall_width / 2 + wall_thickness + corner_radius + tab_height, overall_height + tab_height) \
        .lineTo(-overall_width / 2 + wall_thickness + corner_radius, overall_height) \
        .close() \
        .extrude(-wall_thickness + wall_clearance / 2, combine=False)

    result = result.union(tab)

    return result


def shelf(
        length: float,
        width: float,
        height: float,
        base_width: float,
        base_length: float,
        base_height: float,
        corner_radius: float,
        wall_clearance: float,
        wall_thickness: float,
        ridge_height: float,
        shelf_wall_thickness: float,
        shelf_wall_depth: float,
        tab_height: float,
        **unused_kwargs
):
    overall_width = width * base_width + wall_thickness * 2
    overall_length = length * base_length + wall_thickness * 2
    overall_height = height * base_height + ridge_height + tab_height

    result = cq.Workplane('XY')\
        .box(
            overall_width + shelf_wall_thickness * 2,
            overall_length + shelf_wall_thickness,
            overall_height + shelf_wall_thickness * 2,
            centered=(True, True, False)
        )\
        .translate((0, -shelf_wall_thickness/2, -shelf_wall_thickness))

    inside = cq.Workplane('XY').workplane(offset=-wall_clearance)\
        .box(
            overall_width + wall_clearance * 2,
            overall_length + wall_clearance,
            overall_height + wall_clearance * 2,
            centered=(True, True, False)
        ) \
        .translate((0, -wall_clearance/2, 0))\
        .edges('|Z and < Y')\
        .fillet(corner_radius + wall_thickness + wall_clearance)

    result = result.cut(inside)

    front_tab_stop = cq.Workplane('XZ').workplane(offset=-overall_length/2)\
        .box(overall_width + wall_clearance * 2, tab_height/2, shelf_wall_thickness, centered=(True, False, False))\
        .translate((0, 0, overall_height+wall_clearance-tab_height/2))

    result = result.union(front_tab_stop)

#    z_cutout = cq.Workplane('XY').workplane(offset=-shelf_wall_thickness)\
#        .box(
#            overall_width + shelf_wall_thickness * 2 - shelf_wall_depth * 2,
#            overall_length + shelf_wall_thickness - shelf_wall_depth * 2,
#            overall_height + shelf_wall_thickness * 2,
#            centered=(True, True, False)
#        ) \
#        .translate((0, -shelf_wall_thickness/2, 0)) \
#        .edges('|Z') \
#        .chamfer(shelf_wall_thickness)
#
#    result = result.cut(z_cutout)

    y_cutout = cq.Workplane('XY').workplane(offset=shelf_wall_depth - shelf_wall_thickness)\
        .box(
            overall_width + shelf_wall_thickness * 2 - shelf_wall_depth * 2,
            overall_length + shelf_wall_thickness,
            overall_height + shelf_wall_thickness * 2 - shelf_wall_depth * 2,
            centered=(True, True, False)
        )\
        .translate((0, -shelf_wall_thickness/2, 0))\
        .edges('|Y')\
        .chamfer(shelf_wall_thickness)

    result = result.cut(y_cutout)

#    x_cutout = cq.Workplane('XY').workplane(offset=shelf_wall_depth - shelf_wall_thickness)\
#        .box(
#            overall_width + shelf_wall_thickness * 2,
#            overall_length + shelf_wall_thickness - shelf_wall_depth * 2,
#            overall_height + shelf_wall_thickness * 2 - shelf_wall_depth * 2,
#            centered=(True, True, False)
#        )\
#        .translate((0, -shelf_wall_thickness/2, 0))\
#        .edges('|X')\
#        .chamfer(shelf_wall_thickness)
#
#    result = result.cut(x_cutout)

    return result


def shelves(
        shelf_arrangement: dict,
        length: int,
        base_width: float,
        base_length: float,
        base_height: float,
        corner_radius: float,
        wall_clearance: float,
        wall_thickness: float,
        ridge_height: float,
        shelf_wall_thickness: float,
        shelf_wall_depth: float,
        tab_height: float,
        **unused_kwargs,
):

    min_shelf_width = min(shelf_arrangement, key=lambda a: a[1][0])[1][0]
    min_shelf_height = min(shelf_arrangement, key=lambda a: a[1][1])[1][1]

    shelf_width = (min_shelf_width * base_width + wall_thickness * 2 + shelf_wall_thickness) / min_shelf_width
    shelf_height = (min_shelf_height * base_height + ridge_height + tab_height + shelf_wall_thickness) / min_shelf_height

    result = cq.Workplane('XY')
    for (x, y), (w, h) in shelf_arrangement:

        this_shelf_width = w * base_width + wall_thickness * 2 + shelf_wall_thickness
        extra_width = w * shelf_width - this_shelf_width

        this_shelf_height = h * base_height + ridge_height + tab_height + shelf_wall_thickness
        extra_height = h * shelf_height - this_shelf_height

        s = shelf(
            length,
            w,
            h,
            base_width,
            base_length,
            base_height,
            corner_radius,
            wall_clearance,
            wall_thickness,
            ridge_height,
            shelf_wall_thickness,
            shelf_wall_depth,
            tab_height,
        ).translate((
            x * shelf_width + w * shelf_width / 2 + shelf_wall_thickness / 2,
            0,
            y * shelf_height + shelf_wall_thickness + extra_height / 2,
        ))

        if extra_width > 0:
            s = s.faces('>X').wires().toPending().copyWorkplane(cq.Workplane('YZ')).extrude(extra_width/2)
            s = s.faces('<X').wires().toPending().copyWorkplane(cq.Workplane('YZ')).extrude(-extra_width/2)

        if extra_height > 0:
            s = s.faces('>Z').wires().toPending().copyWorkplane(cq.Workplane('XY')).extrude(extra_height/2)
            s = s.faces('<Z').wires().toPending().copyWorkplane(cq.Workplane('XY')).extrude(-extra_height/2)

        result = result.union(s)

    return result


def section(obj, plane):
    obj.cut(plane.rect(10000, 10000).extrude(10000))

shelves_2x3_1x2 = [
    ((0, 0), (3, 2)),
    ((3, 0), (3, 2)),
    ((0, 2), (2, 1)),
    ((2, 2), (2, 1)),
    ((4, 2), (2, 1)),
    ((0, 3), (2, 1)),
    ((2, 3), (2, 1)),
    ((4, 3), (2, 1)),
]

shelves_2x2 = [
    ((0, 0), (2, 1)),
    ((2, 0), (2, 1)),
    ((0, 1), (2, 1)),
    ((2, 1), (2, 1)),
]

shelves_3x3 = [
    ((0, 0), (2, 1)),
    ((2, 0), (2, 1)),
    ((4, 0), (2, 1)),
    ((0, 1), (2, 1)),
    ((2, 1), (2, 1)),
    ((4, 1), (2, 1)),
    ((0, 2), (2, 1)),
    ((2, 2), (2, 1)),
    ((4, 2), (2, 1)),
]

config = {
    'base_width': 20,
    'base_length': 20,
    'base_height': 15,
    'corner_radius': 2,
    'wall_thickness': 1,
    'wall_clearance': 0.25,
    'bottom_thickness': 1,
    'ridge_width': 1,
    'ridge_height': 2,
    'ridge_angle': 60,
    'ridge_clearance': 0.1,
    'handle_thickness': 1.0,
    'handle_height': 5.0,
    'handle_depth': 6.0,
    'tab_height': 1.0,
    'side_dip': 3.0,
    'shelf_wall_thickness': 3.0,
    'shelf_wall_depth': 6.0,
    'shelf_arrangement': shelves_3x3,
}

# show_object(container(
#     width=1,
#     length=1,
#     height=1,
#     **config
# ))
#
# show_object(container(
#     width=1,
#     length=1,
#     height=1,
#     **config
# ).translate((0, 0, config['base_height'])))
#
# show_object(container(
#     width=1,
#     length=1,
#     height=2,
#     **config
# ).translate((0, config['base_width'] + 2, 0)))

# drawer_obj = drawer(
#     width=2,
#     length=4,
#     height=1,
#     **config,
# )
#
# container_obj = container(
#     width=1,
#     length=4,
#     height=1,
#     **config
# )
#
# small_container_obj = container(
#     width=2,
#     length=1,
#     height=1,
#     **config
# )
#
#
# drawer_container = cq.Assembly(drawer_obj, color=cq.Color(0.75, 0.75, 0.75, 0.5)) \
#     .add(container_obj, loc=cq.Location(cq.Vector(config['base_width'] / 2, 0)), color=cq.Color(0.5, 0.5, 0.5)) \
#     .add(container_obj, loc=cq.Location(cq.Vector(-config['base_width'] / 2, 0)), color=cq.Color(0.5, 0.5, 0.5))
#     #.add(small_container_obj, loc=cq.Location(cq.Vector(0, -config['base_length']/2, config['base_height'])), color=cq.Color(0.5, 0.5, 0.5))
#
# #show_object(drawer_container)
#
# shelf_obj = shelf(
#     width=2,
#     length=4,
#     height=1,
#     **config,
# )
#
# shelf_drawer = cq.Assembly(shelf_obj, color=cq.Color(0.25, 0.25, 0.25))\
#     .add(drawer_container)
#
# show_object(shelf_drawer)

shelves_obj = shelves(length=4, **config)

show_object(shelves_obj)

#show_object(small_container_obj)

# container_obj = container(
#     width=3,
#     length=2,
#     height=2,
#     **config,
# )
#
# container_grid_obj = container_grid(
#     width=9,
#     length=4,
#     **config,
# )
#
# assembly = cq.Assembly(container_obj, color=cq.Color(1.0, 0.0, 0.0))\
#     .add(container_grid_obj, loc=cq.Location(cq.Vector(config['base_width']*0, config['base_length']*0)), color=cq.Color(0.0, 0.0, 1.0)) #
# show_object(assembly)

# cq.exporters.export(small_container_obj, '2x1_container.amf')
# cq.exporters.export(container_obj, '1x4_container.amf')
# cq.exporters.export(drawer_obj, '2x4_drawer.amf')
cq.exporters.export(shelves_obj, 'shelves.amf')
